/**
 * titbit main
 * Copyright (c) [2019.08] BraveWang
*/
'use strict';

let titbit = require('./lib/titbit');

module.exports = titbit;
